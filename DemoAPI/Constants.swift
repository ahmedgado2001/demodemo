//
//  Constants.swift
//  DemoAPI
//
//  Created by ahmed gado on 6/6/19.
//  Copyright © 2019 ahmed gado. All rights reserved.
//

import Foundation
struct Constants {
    
    static let baseURL =  "http://happy.aait-sa.com/api/"
    
    static let allSection = "section/show"
    static let add = "api/service/store"
}
