//
//  BackendManger.swift
//  DemoAPI
//
//  Created by ahmed gado on 6/9/19.
//  Copyright © 2019 ahmed gado. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import ObjectMapper

class BackendMangerr {
    weak var delegate: AddDelegate!
    func getdata(){
        Alamofire.request(AddRouter.add).responseJSON { (response) in
            switch response.result {
            case .success(let value):
                //
                let json = JSON(value)
                break
            case .failure(let error):
                print("Error")
                break
                
            }
        }
    }
    func getSection() {
        Alamofire.request(AddRouter.getSection).responseJSON { (response) in
            switch response.result {
                
            case .success(let value):
                let json = JSON(value)
            let sections = json["data"].arrayValue.compactMap({ Section(map: Map(mappingType: .fromJSON, JSON: $0.dictionaryObject ?? [:] )) })
                self.delegate.didDownLoad(sect: sections)
            case .failure(let error):
                print(error)
                self.delegate.fail(error: error.localizedDescription)
                break
            }
        }
    }
}
