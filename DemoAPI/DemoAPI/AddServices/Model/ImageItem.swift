//
//  ImageItem.swift
//  DemoAPI
//
//  Created by ahmed gado on 6/11/19.
//  Copyright © 2019 ahmed gado. All rights reserved.
//

import Foundation
import UIKit
class AddImage {
    var img:UIImage?
    var imgData:Data?
    var iSButton : Bool
    
    init(img:UIImage?, imgData:Data? , iSButton : Bool) {
        self.img = img
        self.imgData = imgData
        self.iSButton  = iSButton
        
    }
}
