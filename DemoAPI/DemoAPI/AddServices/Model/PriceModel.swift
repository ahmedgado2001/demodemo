//
//  PriceModel.swift
//  DemoAPI
//
//  Created by ahmed gado on 6/9/19.
//  Copyright © 2019 ahmed gado. All rights reserved.
//

import Foundation
import ObjectMapper

struct PriceTabelItem {
    var startDate : String
    var endDate : String
    var price : String
    func toDic()->[String:Any] {
        
        let dic = [
            "start_date": startDate ,
            "end_date": endDate ,
            "price": price ,
            
            ] as [String : Any]
        return dic
    }
    
}

class Price: Mappable{
    private var startDate : String!
    private  var endDate : String!
    private var allPrices : String!
    
    var _startDate: String {
        get {
            if startDate == nil {
                startDate = ""
            }
            return startDate
        } set {
            startDate = newValue
        }
    }
    
    var _endDate :String {
        get{
            if endDate == nil {
                endDate = ""
            }
            return endDate
        }set {
            endDate = newValue
        }
    }
    
    var _allPrices :String {
        get{
            if allPrices == nil {
                allPrices = ""
            }
            return allPrices
        }set{
            allPrices = newValue
        }
    }
    
    init() { }
    required init?(map: Map) {
        
        self.mapping(map: map)
        
    }
    
    func mapping(map: Map) {
        
        _startDate <- map["start_date"]
        _endDate <- map["end_date"]
        _allPrices <- map["price"]
        
    }
    
}
