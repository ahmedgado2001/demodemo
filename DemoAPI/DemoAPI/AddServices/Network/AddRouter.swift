//
//  AddRouter.swift
//  DemoAPI
//
//  Created by ahmed gado on 6/9/19.
//  Copyright © 2019 ahmed gado. All rights reserved.
//

import Foundation
import Alamofire

enum AddRouter : URLRequestConvertible {
    case add
    case getSection
    var method: HTTPMethod {
        switch self {
        case .add:
            return .post
        case .getSection :
            return .post
            
        }
        
    }

    // MARK: - Parameters
//    internal var parameters: Parameters? {
//        var params = Parameters.init()
//        switch self {
//        case .login(let email, let password):
//            params["email"] = email
//            params["password"] = password
//        case .register(let email, let password, let phone):
//            params["email"] = email
//            params["password"] = password
//            params["phone"] = phone
//        }
//        return params
//    }
    
    var paramter : [String:Any]?{
        switch self {
        case .add:
            return [
                "lang" : Language.currentLanguage() ,
                "user_id" : "18" ,
                "section_id" : "3" ,
                "country_id" : "2" ,
                "phone" : "1222" ,
                "quantity" : 0 ,
                "payment_method" :  0 ,
                "lat" : 38973.9 ,
                "lng" : 38973.9 ,
                "title_ar" : "aaaa" ,
                "title_en" : "aaaa" ,
            ]
        case .getSection :
            return [
                "lang" : Language.currentLanguage()
            ]
        }
        
    }
        
        var url : URL {
            let relativePath : String?
            switch self {
            case .add:
                relativePath = Constants.add
            case .getSection :
                relativePath = Constants.allSection
           
        }
            var url = URL(string: Constants.baseURL)!
            if let relativePath = relativePath {
                url = url.appendingPathComponent(relativePath)
            }
            return url
    }
        var encoding : ParameterEncoding {
            switch self {
           
            default:
                return JSONEncoding.default
            }
        }
        func asURLRequest() throws -> URLRequest {
            var urlRequest = URLRequest(url: url)
            urlRequest.httpMethod = method.rawValue
            return try encoding.encode(urlRequest, with: paramter)
        }
    
    
}
