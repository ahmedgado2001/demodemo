//
//  AddServicesVC.swift
//  DemoAPI
//
//  Created by ahmed gado on 6/9/19.
//  Copyright © 2019 ahmed gado. All rights reserved.
//

import UIKit
import Alamofire
import ActionSheetPicker_3_0
protocol AddDelegate: class {
    func didDownLoad(sect: [Section])
    func fail(error: String)
}

class AddServicesVC: UIViewController {
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var addImageButton: UIButton!
    @IBOutlet weak var servButton: UIButton!
    @IBOutlet weak var onlineButton: UIButton!
    @IBOutlet weak var cashButton: UIButton!
    @IBOutlet weak var tabelHeight: NSLayoutConstraint!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var englishTextView: UITextView!
    @IBOutlet weak var arabicTextView: UITextView!
    @IBOutlet weak var quantityAvailableTextField: UITextField!
    @IBOutlet weak var codeButton: UIButton!
    
    @IBOutlet weak var phoneNumberTextFeild: UITextField!
    @IBOutlet weak var codeTextFeild: UITextField!
    @IBOutlet weak var servicesTypeImage: UIImageView!
    @IBOutlet weak var sservicesTypeTextField: UITextField!
    @IBOutlet weak var priceButton: UIButton!
    @IBOutlet weak var imageLocation: UIImageView!
    @IBOutlet weak var locationTextField: UITextField!
    @IBOutlet weak var normalPriceTextField: UITextField!
    @IBOutlet weak var arabicLabel: UILabel!
    @IBOutlet weak var englishTextField: UITextField!
    @IBOutlet weak var arabicTextField: UITextField!
    @IBOutlet weak var imageCollectionView: UICollectionView!
    @IBOutlet weak var priceTabelView: UITableView!
    // collection

    var images = [AddImage]()
    let MyCollectionViewCellId: String = "AddServicesCell"
    let secondCellId : String = "ServicesButtonCell"
    // tabel
    var prices = [PriceTabelItem(startDate: "", endDate: "", price: "")]
    let myTabelViewCellId : String = "PriceTableCell"
    var fromTxt = ""
    var toTxt = ""
    var priceTxt = ""
    var paymentMethod : Int = 2
    var isChoosedPrice = false
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        // Do any additional setup after loading the view.
    }
    func setupView (){
        if Language.currentLanguage().contains("en"){
            backButton.setImage(#imageLiteral(resourceName: "orgempty"), for: UIControl.State.normal)
            
        }else{
            backButton.setImage(#imageLiteral(resourceName: "orgempty"), for: UIControl.State.normal)
            
        }
        arabicTextField.layer.cornerRadius = arabicTextField.frame.height/2
        arabicTextField.layer.borderColor = grayColor.cgColor
        arabicTextField.layer.borderWidth = 1
        arabicTextField.setLeftPaddingPoints(10)
        arabicTextField.setRightPaddingPoints(10)
        
        englishTextField.layer.cornerRadius = englishTextField.frame.height/2
        englishTextField.layer.borderColor = grayColor.cgColor
        englishTextField.layer.borderWidth = 1
        englishTextField.setLeftPaddingPoints(10)
        englishTextField.setRightPaddingPoints(10)
        
        normalPriceTextField.layer.cornerRadius = normalPriceTextField.frame.height/2
        normalPriceTextField.layer.borderColor = grayColor.cgColor
        normalPriceTextField.layer.borderWidth = 1
        normalPriceTextField.setLeftPaddingPoints(10)
        normalPriceTextField.setRightPaddingPoints(10)
        normalPriceTextField.keyboardType = .asciiCapableNumberPad
        
        locationTextField.layer.cornerRadius = locationTextField.frame.height/2
        locationTextField.layer.borderColor = grayColor.cgColor
        locationTextField.layer.borderWidth = 1
        locationTextField.setLeftPaddingPoints(10)
        locationTextField.setRightPaddingPoints(10)
        
        imageLocation.image = #imageLiteral(resourceName: "orgempty")
        priceButton.layer.cornerRadius = priceButton.frame.height/2
        
        sservicesTypeTextField .layer.cornerRadius = sservicesTypeTextField.frame.height/2
        sservicesTypeTextField.layer.borderColor = grayColor.cgColor
        sservicesTypeTextField.layer.borderWidth = 1
        sservicesTypeTextField.setLeftPaddingPoints(10)
        sservicesTypeTextField.setRightPaddingPoints(10)
    
        codeTextFeild.layer.cornerRadius = codeTextFeild.frame.height/2
        codeTextFeild.layer.borderColor = grayColor.cgColor
        codeTextFeild.layer.borderWidth = 1
        
        phoneNumberTextFeild.layer.cornerRadius = phoneNumberTextFeild.frame.height/2
        phoneNumberTextFeild.layer.borderColor = grayColor.cgColor
        phoneNumberTextFeild.layer.borderWidth = 1
        phoneNumberTextFeild.setLeftPaddingPoints(10)
        phoneNumberTextFeild.setRightPaddingPoints(10)
        phoneNumberTextFeild.keyboardType = .phonePad
        codeButton.setImage(#imageLiteral(resourceName: "orgempty"), for: .normal)
        
        quantityAvailableTextField.layer.cornerRadius = quantityAvailableTextField.frame.height/2
        quantityAvailableTextField.layer.borderColor = grayColor.cgColor
        quantityAvailableTextField.layer.borderWidth = 1
        quantityAvailableTextField.setLeftPaddingPoints(10)
        quantityAvailableTextField.setRightPaddingPoints(10)
        quantityAvailableTextField.keyboardType = .asciiCapableNumberPad
        
        
        arabicTextView.layer.borderWidth = 0.5
        arabicTextView.layer.cornerRadius = 15
        arabicTextView.layer.borderColor = grayColor.cgColor
        arabicTextView.layer.borderWidth = 1
        arabicTextView.textContainerInset =
            UIEdgeInsets(top: 8,left: 10,bottom: 8,right: 5)
        
        englishTextView.layer.cornerRadius = 15
        englishTextView.layer.borderWidth = 0.5
        englishTextView.layer.borderColor = grayColor.cgColor
        englishTextView.layer.borderWidth = 1
        englishTextView.textContainerInset =
            UIEdgeInsets(top: 8,left: 10,bottom: 8,right: 5)
        
        addButton.layer.cornerRadius = addButton.frame.height/2
     
//        
        self.priceTabelView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        
        cashButton.setImage(#imageLiteral(resourceName: "orgempty"), for: UIControl.State.normal)
        onlineButton.setImage(#imageLiteral(resourceName: "orgempty"), for: UIControl.State.normal)
        

        servButton.setImage(#imageLiteral(resourceName: "orgempty"), for: UIControl.State.normal)
        
        
        arabicTextView.text = "Description the service provided to users"
//        arabicTextView.delegate = self
        arabicTextView.textColor = UIColor.lightGray
        
        
        englishTextView.text = "Description the service provided to users"
//        englishTextView.delegate = self
        englishTextView.textColor = UIColor.lightGray
        
     
        imageCollectionView.delegate = self
        imageCollectionView.dataSource = self
        imageCollectionView.register(UINib(nibName: MyCollectionViewCellId, bundle: nil), forCellWithReuseIdentifier: MyCollectionViewCellId)
        
        imageCollectionView.register(UINib(nibName: secondCellId, bundle: nil), forCellWithReuseIdentifier: secondCellId)
        
        self.images.append(AddImage(img: nil, imgData: nil, iSButton: true))

        priceTabelView.dataSource = self
        priceTabelView.delegate = self
        priceTabelView.register(UINib(nibName: myTabelViewCellId, bundle: nil), forCellReuseIdentifier: myTabelViewCellId)
    }
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        priceTabelView.layer.removeAllAnimations()
        tabelHeight.constant = priceTabelView.contentSize.height
        
        
        UIView.animate(withDuration: 0.5) {
            self.view.updateConstraints()
            self.view.layoutIfNeeded()
        }
        
    }
    
    @IBAction func addPriceButtonPressed(_ sender: Any) {
//
        prices.append(PriceTabelItem(startDate: "", endDate: "", price: ""))
        priceTabelView.reloadData()
    }
 

}
extension  AddServicesVC : AddDelegate {
    func didDownLoad(sect: [Section]) {
    // array of section
        
    }
    
    func fail(error: String) {
        print(error)
    }
}
extension AddServicesVC : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = images[indexPath.row]
        
        if item.iSButton == true {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: secondCellId, for: indexPath) as! ServicesButtonCell
            cell.addAction = {
                let imagePickerController = UIImagePickerController()
                imagePickerController.delegate = self
//                imagePickerController.view.tag = 1
                let actionSheet = UIAlertController(title: "Add photo", message: "choose a sourse", preferredStyle: .actionSheet)
                actionSheet.addAction(UIAlertAction(title: "camera", style: .default, handler: { (action:UIAlertAction) in
                    if UIImagePickerController.isSourceTypeAvailable(.camera){
                        imagePickerController.sourceType = .camera
                        
                        self.present(imagePickerController, animated: true, completion: nil)
                    }else{
                        //show alert
                        let alert = UIAlertController(title: "camera Dont work", message: "Restart Your App", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Back", style: .default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        print("No camera")
                    }
                    
                    
                    
                }))
                actionSheet.addAction(UIAlertAction(title: "photoLibrary", style: .default, handler: { (action:UIAlertAction) in
                    imagePickerController.sourceType = .photoLibrary
                    imagePickerController.allowsEditing = true
                    
                    self.present(imagePickerController, animated: true, completion: nil)
                    
                }))
                actionSheet.addAction(UIAlertAction(title: "cancel", style: .cancel, handler: nil))
                self.present(actionSheet, animated: true, completion: nil)
                
            }
            return cell
            
        }else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MyCollectionViewCellId, for: indexPath) as! AddServicesCell
            cell.configerCell(item: item)
            cell.removeAction = {
                self.images.remove(at: indexPath.row)
                self.imageCollectionView.reloadData()
            }
            
            return cell
        }
        
    }
    //---------------------size of cell
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        return CGSize(width: 120  , height: 120)
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
}
extension AddServicesVC : UIImagePickerControllerDelegate ,UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        var selectedImage: UIImage?
        if let editedImage = info[.editedImage] as? UIImage {
            selectedImage = editedImage
            let imageData:Data = (selectedImage!).pngData()! as Data
            self.images.append(AddImage(img: selectedImage!, imgData: imageData, iSButton: false))
            self.imageCollectionView.reloadData()
            picker.dismiss(animated: true, completion: nil)
        } else if let originalImage = info[.originalImage] as? UIImage {
            selectedImage = originalImage
            let imageData:Data = (selectedImage!).pngData()! as Data
            self.images.append(AddImage(img: selectedImage!, imgData: imageData, iSButton: false))
            self.imageCollectionView.reloadData()
            
            picker.dismiss(animated: true, completion: nil)
        }
        
    }
    
}
extension AddServicesVC :  UITableViewDelegate , UITableViewDataSource  {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return   prices.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: myTabelViewCellId) as! PriceTableCell
        let item = prices[indexPath.row]
        cell.configerCell(item: item)
        cell.fromAction = {
            self.isChoosedPrice = true
            
            self.prices[indexPath.row].startDate = cell.fromTextField.text!
            self.prices[indexPath.row].endDate = cell.toTextField.text!
            self.prices[indexPath.row].price = cell.priceTextField.text!
            self.fromTxt = cell.fromTextField.text!
            self.toTxt = cell.toTextField.text!
            self.priceTxt =  cell.priceTextField.text!
            
            
            //            if cell.first.compare(cell.second) == .orderedAscending {
            //                print("First Date is smaller then second date")
            //            }
            
        }
        
        cell.toAction = {
            self.isChoosedPrice = true
            
            self.prices[indexPath.row].startDate = cell.fromTextField.text!
            self.prices[indexPath.row].endDate = cell.toTextField.text!
            self.prices[indexPath.row].price = cell.priceTextField.text!
            self.fromTxt = cell.fromTextField.text!
            self.toTxt = cell.toTextField.text!
            self.priceTxt =  cell.priceTextField.text!
            
            
            if cell.first.compare(cell.second) == .orderedDescending {
                return
                
            }else{
                cell.toTextField.text = String(describing: cell.secondDate)
            }
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let delete = UIContextualAction(style: .normal, title: "delete"){(action, view , completion) in
            if indexPath.row != 0 {
                self.prices.remove(at: indexPath.row)
                self.priceTabelView.reloadData()
            }
            
        }
        delete.backgroundColor =  UIColorFromRGB(rgbValue: 0xE0342F)
        delete.image = #imageLiteral(resourceName: "deleteicon")
        
        if indexPath.row == 0 {
            return UISwipeActionsConfiguration(actions :[])
            
        }else{
            return UISwipeActionsConfiguration(actions :[delete])
        }
        
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        return UISwipeActionsConfiguration(actions :[])
        
    }
    
    
    
}
