//
//  AddServicesCell.swift
//  DemoAPI
//
//  Created by ahmed gado on 6/11/19.
//  Copyright © 2019 ahmed gado. All rights reserved.
//

import UIKit

class AddServicesCell: UICollectionViewCell {
    
    
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var ImageCell: UIImageView!
    var removeAction : (()->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        deleteButton.setImage(#imageLiteral(resourceName: "orgempty"), for: UIControl.State.normal)
        // Initialization code
    }
    func configerCell(item: AddImage){
        ImageCell.image = item.img
    }
   
    @IBAction func buttonPressed(_ sender: Any) {
        removeAction?()
    }
    

}
