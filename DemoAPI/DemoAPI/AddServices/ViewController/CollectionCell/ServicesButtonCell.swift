//
//  ServicesButtonCell.swift
//  DemoAPI
//
//  Created by ahmed gado on 6/11/19.
//  Copyright © 2019 ahmed gado. All rights reserved.
//

import UIKit

class ServicesButtonCell: UICollectionViewCell {
    @IBOutlet weak var addButton: UIButton!
    var addAction : (()->())?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code\
        addButton.setImage(#imageLiteral(resourceName: "orgempty"), for: UIControl.State.normal)
        
    }
    @IBAction func addButtonPressed(_ sender: Any) {
        addAction?()
    }
}
