//
//  PriceTableCell.swift
//  DemoAPI
//
//  Created by ahmed gado on 6/9/19.
//  Copyright © 2019 ahmed gado. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
class PriceTableCell: UITableViewCell {

    @IBOutlet weak var toButton: UIButton!
    @IBOutlet weak var fromButton: UIButton!
    @IBOutlet weak var toTextField: UITextField!
    @IBOutlet weak var fromTextField: UITextField!
    @IBOutlet weak var priceTextField: UITextField!
    
    @IBOutlet weak var toLabel: UILabel!
    @IBOutlet weak var fromLabel: UILabel!
    @IBOutlet weak var spacificPriceLabel: UILabel!
    
    var priceAction : (()->())?
    var toAction   : (()->())?
    var fromAction : (()->())?
    var first = Date()
    var second = Date()
    var firstDate = ""
    var secondDate = ""
    
    var isFromDateSelected = false
    var isToDateSelected = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        if Language.currentLanguage().contains("en") {
            spacificPriceLabel.textAlignment = .left
            
            
        }else{
            spacificPriceLabel.textAlignment = .right
            
        }
        spacificPriceLabel.text = "   Specific price"
        fromLabel.text = "     From"
        toLabel.text = "     To"
        
        
        // Initialization code
        priceTextField.layer.cornerRadius = priceTextField.frame.height/2
        priceTextField.layer.borderColor = grayColor.cgColor
        priceTextField.layer.borderWidth = 1
        priceTextField.keyboardType = .asciiCapableNumberPad
        priceTextField.setLeftPaddingPoints(10)
        priceTextField.setRightPaddingPoints(10)
        
        fromTextField.layer.cornerRadius = fromTextField.frame.height/2
        fromTextField.layer.borderColor = grayColor.cgColor
        fromTextField.layer.borderWidth = 1
        fromTextField.setLeftPaddingPoints(10)
        fromTextField.setRightPaddingPoints(10)
        
        toTextField.layer.cornerRadius = toTextField.frame.height/2
        toTextField.layer.borderColor = grayColor.cgColor
        toTextField.layer.borderWidth = 1
        toTextField.setLeftPaddingPoints(10)
        toTextField.setRightPaddingPoints(10)
        //         priceTextField.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(editText(_:))))
        priceTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControl.Event.editingDidEnd)
    }
    @objc func textFieldDidChange(_ textField: UITextField) {
        self.fromAction?()
    }
    func configerCell( item : PriceTabelItem){
        toTextField.text = item.endDate
        fromTextField.text = item.startDate
        priceTextField.text = item.price
    }
    
    @IBAction func fromButtonPressed(_ sender: Any) {
        let datePicker = ActionSheetDatePicker(title: "Select date", datePickerMode: UIDatePicker.Mode.date, selectedDate: Date(), doneBlock: {
            picker, value, index in
            
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            formatter.locale = Locale(identifier: "en")
            let firstDate = formatter.string(from: value! as! Date)
            
            //            let secondDate = formatter.string(from: value! as! Date)
            self.isFromDateSelected = true
            self.isToDateSelected = false
            self.toTextField.text = ""
            self.first = value! as! Date
            
            self.fromTextField.text = String(describing: firstDate)
            
            self.fromAction?()
            
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: (sender as AnyObject).superview!.superview)
        let secondsInWeek: TimeInterval = 7 * 24 * 60 * 60;
        datePicker?.minimumDate = Date()
        datePicker?.minuteInterval = 20
        datePicker?.locale = Locale(identifier: "en")
        
        let bar1 = UIBarButtonItem.init(title: "Done", style: .plain, target: self, action: #selector(actionPickerDone))
        let bar2 = UIBarButtonItem.init(title: "Cancel", style: .plain, target: self, action: #selector(actionPickerCancel))
        bar1.setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "Cairo-Regular", size: 20) as Any, NSAttributedString.Key.foregroundColor: UIColor.orange], for: .normal)
        bar2.setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "Cairo-Regular", size: 20) as Any, NSAttributedString.Key.foregroundColor: UIColor.orange], for: .normal)
        datePicker?.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "Cairo-Regular", size: 20) as Any]
        datePicker!.minuteInterval = 1
        
        datePicker?.setDoneButton(bar1)
        datePicker?.setCancelButton(bar2)
        datePicker?.pickerTextAttributes = [NSAttributedString.Key.font: UIFont(name: "Cairo-Regular", size: 20) as Any]
        datePicker?.show()
    }
    
    @IBAction func toButtonPressed(_ sender: Any) {
        //        toAction?()
        let datePicker = ActionSheetDatePicker(title: "Select date", datePickerMode: UIDatePicker.Mode.date, selectedDate: Date(), doneBlock: {
            picker, value, index in
            
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            let secondDate = formatter.string(from: value! as! Date)
            self.secondDate = secondDate
            //            self.isToDateSelected = true
            self.second = value! as! Date
            self.toTextField.text = String(describing: secondDate)
            self.toAction?()
            
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: (sender as AnyObject).superview!.superview)
        let secondsInWeek: TimeInterval = 7 * 24 * 60 * 60;
        datePicker?.minimumDate = Date()
        datePicker?.minuteInterval = 20
        datePicker?.locale = Locale(identifier: "en")
        
        let bar1 = UIBarButtonItem.init(title: "Done", style: .plain, target: self, action: #selector(actionPickerDone))
        let bar2 = UIBarButtonItem.init(title: "Cancel", style: .plain, target: self, action: #selector(actionPickerCancel))
        bar1.setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "Cairo-Regular", size: 20) as Any, NSAttributedString.Key.foregroundColor: UIColor.orange], for: .normal)
        bar2.setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "Cairo-Regular", size: 20) as Any, NSAttributedString.Key.foregroundColor: UIColor.orange], for: .normal)
        datePicker?.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "Cairo-Regular", size: 20) as Any]
        datePicker!.minuteInterval = 1
        
        datePicker?.setDoneButton(bar1)
        datePicker?.setCancelButton(bar2)
        datePicker?.pickerTextAttributes = [NSAttributedString.Key.font: UIFont(name: "Cairo-Regular", size: 20) as Any]
        
        
        datePicker?.show()
    }
    @objc func actionPickerCancel(){
        
    }
    
    @objc func actionPickerDone(){
        
    }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
