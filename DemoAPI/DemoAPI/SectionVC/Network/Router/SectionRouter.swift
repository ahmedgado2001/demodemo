//
//  SectionRouter.swift
//  DemoAPI
//
//  Created by ahmed gado on 6/6/19.
//  Copyright © 2019 ahmed gado. All rights reserved.
//

import Foundation
import Alamofire
enum SectionRouter : URLRequestConvertible {
    case getSection
    
    var method: HTTPMethod {
        switch self {
        case .getSection:
            return .post
     
        }
        
    }
    
    var paramter : [String:Any]? {
    
        return [
            "lang" : Language.currentLanguage()
        ]
    }
    
    var url : URL {
        let relativePath : String?
        switch self {
        case .getSection:
            relativePath = Constants.allSection
        
    }
        var url = URL(string: Constants.baseURL)!
        if let relativePath = relativePath {
            url = url.appendingPathComponent(relativePath)
        }
        return url
}
    
    var encoding: ParameterEncoding {
        switch self {
        default :
            return JSONEncoding.default
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method.rawValue
        return try encoding.encode(urlRequest, with: paramter)
    }

}
