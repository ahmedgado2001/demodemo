//
//  BackendMager.swift
//  DemoAPI
//
//  Created by ahmed gado on 6/6/19.
//  Copyright © 2019 ahmed gado. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import ObjectMapper
class BackendManger {
    
    weak var delegate: SectionDelegate!

    func getdata(){
        // MARK:- Using URLRequestConvertible
        Alamofire.request(SectionRouter.getSection).responseJSON { (response) in
            switch response.result {
            case .success(let value) :
                let json = JSON(value)
                                
                let sections = json["data"].arrayValue.compactMap({ Section(map: Map(mappingType: .fromJSON, JSON: $0.dictionaryObject ?? [:] )) })
                self.delegate.didDownLoad(sect: sections )

            case .failure(let error) :
                self.delegate.fail(error: error.localizedDescription)
                break
            }
        }
    }
    
}
