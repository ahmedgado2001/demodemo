//
//  SectionItem.swift
//  DemoAPI
//
//  Created by ahmed gado on 6/6/19.
//  Copyright © 2019 ahmed gado. All rights reserved.
//

import Foundation
import ObjectMapper

class Section: Mappable {
    
    private var title: String!
    private var image: String!
    private var id: Int!
 
    
    var _id: Int {
        get {
            if id == nil {
                id = 0
            }
            return id
        } set {
            id = newValue
        }
    }
    
    var _title: String {
        get {
            if title == nil {
                title = ""
            }
            return title
        } set {
            title = newValue
        }
    }
    
    var _image: String {
        get {
            if image == nil {
                image = ""
            }
            return image
        } set {
            image = newValue
        }
    }
    
    init() { }
    required init?(map: Map) {
        
        self.mapping(map: map)
        
    }
    
    func mapping(map: Map) {
        
       
        _id <- map["id"]
        _title <- map["title"]
        _image <- map["image"]
        
    }
    
}
