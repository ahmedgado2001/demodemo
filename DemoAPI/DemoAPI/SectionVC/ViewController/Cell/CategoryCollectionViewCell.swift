//
//  CategoryCollectionViewCell.swift
//  DemoAPI
//
//  Created by ahmed gado on 6/7/19.
//  Copyright © 2019 ahmed gado. All rights reserved.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var sectionName: UILabel!
    @IBOutlet weak var sectionImage: UIImageView!
    @IBOutlet weak var borderImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        borderImage.image = #imageLiteral(resourceName: "orgempty")
        sectionImage.layer.cornerRadius = sectionImage.frame.height/2
        sectionImage.clipsToBounds = true
        // Initialization code
    }
    
}
