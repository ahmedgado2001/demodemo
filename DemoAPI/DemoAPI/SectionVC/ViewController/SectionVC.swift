//
//  SectionVC.swift
//  DemoAPI
//
//  Created by ahmed gado on 6/6/19.
//  Copyright © 2019 ahmed gado. All rights reserved.
//

import UIKit
protocol SectionDelegate: class {
    func didDownLoad(sect: [Section])
    func fail(error: String)
}
class SectionVC: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    let MyCollectionViewCellId: String = "CategoryCollectionViewCell"
    var sections = [Section]()
    var backEndManger = BackendManger()

    override func viewDidLoad() {
        super.viewDidLoad()
        backEndManger.delegate = self

        collectionView.delegate = self
        collectionView.dataSource = self
        // Do any additional setup after loading the view.
        collectionView.register(UINib(nibName: MyCollectionViewCellId, bundle: nil), forCellWithReuseIdentifier: MyCollectionViewCellId)
        backEndManger.getdata()

    }
    

    @IBAction func addButtonPressed(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "AddServicesVC") as! AddServicesVC
        navigationController?.pushViewController(vc, animated: true)
    }
    

}
extension  SectionVC : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sections.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MyCollectionViewCellId, for: indexPath) as! CategoryCollectionViewCell
        cell.sectionImage.setImageWith(sections[indexPath.row]._image)
        cell.sectionName.text = sections[indexPath.row]._title
        return cell
    }
    
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        let ScreenWidth = UIScreen.main.bounds.width

        return CGSize(width: (ScreenWidth/2)-20, height: (ScreenWidth/2)-20 + 30)
        
        
    }
}
extension SectionVC: SectionDelegate {
    
    func didDownLoad(sect: [Section]) {
        self.sections = sect
        collectionView.reloadData()
    }
    
    func fail(error: String) {
        print(error)
    }
}
